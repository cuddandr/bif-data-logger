//Arduino / ATMEGA328 based data logger v1.0
//Author: Andrew Cudd (cuddandr@msu.edu)
//Date  : 2016/08/22
////////////////////////////////////////////

#include "Arduino.h"
#include "EEPROM.h"
#include "SPI.h"
#include "SD.h"

//Define Chip Select lines for the various SPI devices
#define CS_ADC 10 //External ADC
#define CS_SDC 9 //SD Card

//Define pins for status LEDS
#define LED_ADC A0
#define LED_SDC A1

//Define pin for data switch
#define DATA_SWITCH A2

//Define address for keeping track of filenames
#define FILE_NUM_ADDRESS 0x0000

//Constants for the ADC. The v_ref might need to be updated occasionally.
const float v_ref = 5.07; //5.139; //As measured on the 5V pin
const float adc_precision = 4096.0; //12-Bit ADC precision

//Variables for various event flags and timing data
uint8_t take_data_flag = 0;
uint8_t start_data_flag = 0;
uint16_t time_elapsed_ms = 0; //Maximum time is ~1197 hours for one continuous run

//Variables for file naming. Due to the limitations of the SD library,
//file names can be no longer than 8 characters plus 3 for the extension.
//(e.g. filename.ext)
String data_file_pre = "BIF";
String data_file_ext = ".log";
String data_file_name = "";
uint16_t data_file_num = 0;

uint16_t read_adc(uint8_t channel)
{
    //The ADC expects a command byte with a start bit, followed by a bit
    //for the mode (single vs differential) and the channel to read. After
    //the command byte is sent, two null bits are returned by the ADC followed
    //by the 12 bit reading.
    uint16_t adc_value = 0;

    //Set the command byte, the leading zero is to align the byte with the 
    //two null bits returned by the ADC before the 12 bit value
    uint8_t adc_command = B01100000;
    adc_command |= (channel<<2);

    //Send the command and read the value via SPI. The ADC does not care what
    //bits we send it while it returns the 12 bit reading.
    digitalWrite(CS_ADC, LOW);
    SPI.transfer(adc_command);
    adc_value = SPI.transfer16(0x0000);
    digitalWrite(CS_ADC, HIGH);

    //The value is read in MSB first by the SPI command, thus bit shift right
    //to align the 12-bit value
    return adc_value >> 4;
}

float read_adc_voltage(uint8_t channel)
{
    //Read ADC value and convert to voltage
    uint16_t adc_val = read_adc(channel);
    float voltage = (adc_val / adc_precision) * v_ref;
    return voltage;
}

void write_file_header(File file_name)
{
    //Header for a CSV formatted file.
    if(file_name)
        file_name.print("Voltage, Time, Channel\n");
    else
        Serial.println("File Error");
}

// void write_data(float base, int exponent, unsigned int ms)
void write_file_data(File file_name, float voltage, uint16_t ms, uint8_t channel)
{
    //If the file is available, write the data. Otherwise send error message.
    if(file_name) 
    {
        //Turn on status LED and write data formatted as CSV
        digitalWrite(LED_SDC, HIGH);
        file_name.print(voltage, 4);
        file_name.print(", ");
        file_name.print(ms);
        file_name.print(", ");
        file_name.print(channel);
        file_name.print("\n");
        //file_name.close();
        digitalWrite(LED_SDC, LOW);
    }
    //If the file is not open, output error
    else 
    {
        Serial.println("File Error");
    }
}

uint16_t samples_per_second()
{
    //Read the inputs to PORTD (Pins 0-7)
    //Shift right two bits to align the value
    uint8_t sps = PIND >> 2;
    sps = sps ^ B00111111; //XOR bits to change from active low to active high
    uint16_t t_del = 1000 / (sps + 1);
    return t_del;
}

uint8_t read_data_switch()
{
    //Read value from data switch, XOR the LSB since the switch is
    //active low and I still want 1 to be TRUE, and 0 to be FALSE.
    uint8_t temp_reading = digitalRead(DATA_SWITCH);
    return temp_reading ^ 0x01;
}

String update_file_name(uint16_t inc)
{
    //Get the most recent file number and increment it if flag is set
    EEPROM.get(FILE_NUM_ADDRESS, data_file_num);
    if(inc > 0)
        data_file_num += 1; 

    //Save the new file number
    //EEPROM.put(FILE_NUM_ADDRESS, 0x0000);
    EEPROM.put(FILE_NUM_ADDRESS, data_file_num);

    //Return full file name
    return data_file_pre + data_file_num + data_file_ext;
}

void setup()
{
    //Port D: Digital pins 0 - 7
    //Setting a bit to 1 for output, 0 for input in the DDR register
    DDRD &= B00000011; //Set D2 - D7 to input
    PORTD |= B11111100; //Enable D2 - D7 internal pullup resistor

    //Port C: Analog pins 0 - 5
    DDRC &= B11111011; //Set A2 to input
    DDRC |= B00000011; //Set A1, A0 to output
    PORTC |= B00000100; //Enable A2 internal pullup

    //Port B: Digital pins 8 - 13
    DDRB |= B00000111; //Set D8 - D10 to input
    PORTB |= B00000111; //Enable D8 - D10 interal pullup resistor

    //Initialize both SPI hardware and SD interface library
    SPI.begin();
    SD.begin(CS_SDC);

    //Initialize log file name
    //EEPROM.put(FILE_NUM_ADDRESS, 0x0000);
    data_file_name = update_file_name(0);

    //Initialize serial communication over USB
    Serial.begin(9600);
}

void loop()
{
    take_data_flag = read_data_switch();

    //If a data run finished, increment the file name and reset start_data_flag
    if(start_data_flag == 1 && take_data_flag == 0)
    {
        data_file_name = update_file_name(1);
        start_data_flag = 0;
    }

    if(take_data_flag > 0)
    {
        uint16_t t_b = millis();
        digitalWrite(LED_ADC, HIGH);
        uint16_t t_del = samples_per_second();
        File data_file = SD.open(data_file_name, FILE_WRITE);

        //If this is the start of a data run, write header to file and reset timer
        if(start_data_flag == 0)
        {
            write_file_header(data_file);
            start_data_flag = 1;
            time_elapsed_ms = 0;
        }
        
        //Read ADC value, calculate voltage, write to file
        float voltage = read_adc_voltage(0);
        write_file_data(data_file, voltage, time_elapsed_ms, 0);
        voltage = read_adc_voltage(1);
        write_file_data(data_file, voltage, time_elapsed_ms, 1);
        data_file.close();
        digitalWrite(LED_ADC, LOW);
        
        uint16_t t_d = millis() - t_b;
        Serial.println(t_d);
        time_elapsed_ms += t_del + 25;
        delay(t_del);
    }

    //Serial.println(voltage, 4);
    //Serial.println(take_data_flag);
    //Serial.println(start_data_flag);
    Serial.println(data_file_name);
    //Serial.println(EEPROM.read(FILE_NUM_ADDRESS));
    //Serial.println(PIND ^ B11111100, BIN);
    //Serial.println(time_elapsed_ms);

    //delay(5000);
}