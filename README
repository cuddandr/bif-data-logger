This is the README and documentation for the Arduino / ATMEGA328 based data logger,
written by Andrew Cudd (cuddandr@msu.edu). Last modified 2016/08/26.

The data logger is a microcontroller (ATMEGA328) based system utilizing an 8 channel,
12 bit external ADC to take measurements. The measurements are taken at a variable
sample rate and written to a micro SD card as a CSV formatted text file.

The code is available in a Git repo at: https://gitlab.com/cuddandr/bif-data-logger
The repository is setup for PlatformIO. If you need only the code which is loaded
on the board, it is located in /src as main.cpp

Normal Operation:

    The board needs to be powered via a Mini-USB (not Micro-USB) cable or a barrel-jack
    DC souce between 7 and 12 volts. Once powered the device initializes a few services
    and waits for the signal to take data. The toggle switch on the upper board starts
    and stops the data taking; the system will only take and write data while the toggle
    switch is in the on position.

    Each data run is written to a separate file defined by a set of constants in the code.
    The file name is currently comprised of a prefix, an extention, and a number which is 
    incremented for each run. Due to limitations of the SD card library used, file names
    have a maximum size of 8 characters plus 3 characters for the extention. For example,
    filename.ext and file.ext are valid but longerfilename.ext or filename.extention are
    not valid file names.

    Data is stored on a micro SD card which is attached to the system. The micro SD card
    must be formatted as a FAT12, FAT16, or FAT32 file system to work. When removing and
    inserting the SD card the system must be reset before taking data for the files to be 
    correctly written to the SD card. There are two reset buttons available to reset the 
    system. This is obvious, but do not remove or insert the SD card during a data run.

    The external ADC has 8 available channels to take data. The allowed voltage range for
    the ADC is 0 - 5V. If the voltage you wish to measure is greater than 5V (or less than
    0V) you must convert it to fit in the 0 - 5V range. A simple voltage divider can work
    for this purpose.

    The sample rate for the system can be set during operation via the 6 DIP switches on 
    the upper board. The switch has a side labelled open, which is a logical zero, and a
    side labelled with numbers, which is logical one. The 6 switches form a 6-bit word
    which set the samples per second rate. Switch 1 is the MSB and switch 6 being the LSB.

External ADC:

    The external ADC is a MCP3208 chip, which is a 8 channel 12-bit ADC which communicates
    via SPI. The ADC accepts voltages in the range of 0 to 5V and calculates the voltage on
    a given channel with respect to the voltage supplied to the V_REF pin. The ADC calculates
    the voltage as follows:

        V = (V_measured / 4096) * V_reference
    
    The 4096 is 2^12 which is the precision of the ADC. The ADC value is simply the ratio of
    the measured voltage on a given channel to the supplied voltage on the V_REF pin. V_REF is
    simply a constant in the code, so it may have to be updated occasionally to give accurate
    results -- especially if the power supply changes.

SD Card:

    The system saves the data to a microSD card via the SPI interface on the microcontroller.
    Due to the library used, there are few limitations to using the SD card as mentioned above.
    Currently the SD card is the biggest bottleneck in speed, taking about 25 milliseconds to
    complete a write to the card.
    
    Because of the limitations of the 8.3 filenames, currently the maximum number of data runs
    that can be saved is 100000. This is because the code builds the filename from the same
    name and simply increments a number for each data run. One could just change the base file
    name to write more files, or the number appended to each file can be set to zero to reset
    the file count.
    
    The SD card reader is a seperate board which has to be connected by jumpers to the main board.
    Connection requires six wires: VCC, GND, DI, DO, SDK, and CS (the last pin, CD, is not used).
    The pins should be connected as follows:
    
        SD CARD | Arduino Board
        VCC     - 5V
        GND     - GND
        DI      - MOSI: Digital Pin 11 (D11)
        DO      - MISO: Digital Pin 12 (D12)
        SCK     - SCK : Digital Pin 13 (D13)
        CS      - CS  : Digital Pin 9 (D9)
        CD      - Not Used. Leave Unconnected.
        
Programming:

    The system is programmed via a Mini-USB (not Micro-USB) cable and a computer with the
    necessary software. The code is written in C++, but can also be written in C. There
    are two main ways of compiling and uploading code (that I will present): the Arduino IDE
    or a Python program called PlatformIO. I used PlatformIO for this project, but I've also
    used the Arduino IDE for other projects; it should just be a matter of preference.
    
    The board used is a Sparkfun Redboard, which is an slightly modified Arduino clone, but
    is functionally identical to an Aruduino UNO. Below is the Sparkfun guide on the Redboard
    specifically (which is quite good).
    
    I will first defer to the documentation on how to program provided by each project:
    Arduino    - https://www.arduino.cc/en/Guide/HomePage
    Sparkfun   - https://learn.sparkfun.com/tutorials/redboard-hookup-guide
    PlatformIO - http://docs.platformio.org/en/stable/
    
    I believe the Arduino IDE is easier to setup and definitely has more resources than PlatformIO,
    but PlatformIO is more flexible.
