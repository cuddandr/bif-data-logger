#include "Arduino.h"

#define CS 10
#define MOSI 11
#define MISO 12
#define CLK 13

void setup()
{
    pinMode(CS, OUTPUT);
    pinMode(MOSI, OUTPUT);
    pinMode(MISO, INPUT);
    pinMode(CLK, OUTPUT);

    digitalWrite(CS, HIGH);
    digitalWrite(MOSI, LOW);
    digitalWrite(CLK, LOW);

    Serial.begin(9600);
}

unsigned int read_adc(byte channel)
{
    unsigned int adc_value = 0;
    byte adc_command = B11000000;
    adc_command |= (channel<<3);

    digitalWrite(CS, LOW);
    for(byte i=7; i>0; i--)
    {
        digitalWrite(MOSI, adc_command&1<<i);
        digitalWrite(CLK, HIGH);
        digitalWrite(CLK, LOW);
    }

    for(byte j=11; j>0; j--)
    {
        adc_value |= digitalRead(MISO) << j;
        digitalWrite(CLK, HIGH);
        digitalWrite(CLK, LOW);
    }

    digitalWrite(CS, HIGH);

    return adc_value;
}

void loop()
{
    unsigned int val = read_adc(0);
    //val = (val / 4096.0) * 5.139;
    //Serial.println(val, BIN);
    Serial.println(val, DEC);
    Serial.println("Test");
    delay(1000);
}