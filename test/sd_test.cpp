#include "Arduino.h"
#include "SPI.h"
#include "SD.h"

#define chipSelect 8
void setup()
{
  // Open serial communications and wait for port to open:
    Serial.begin(9600);
    Serial.println("Init SD card");
    SD.begin(chipSelect);

    DDRD |= B11111100;
    PORTD |= B11111100;
}

void loop()
{
    byte portState = PIND;

    File dataFile = SD.open("datalog.txt", FILE_WRITE);
    String dataString = "Test!";

    // if the file is available, write to it:
    if(dataFile) 
    {
        dataFile.println(dataString);
        dataFile.close();
        // print to the serial port too:
        Serial.println(dataString);
    }
    // if the file isn't open, pop up an error:
    else 
    {
        Serial.println("Error Opening");
    }
    Serial.println(portState, BIN);
    delay(5000);
}